## Projeto Exemplo - VSCode

- Este é um exemplo de projeto Scanner-Parser feito no VSCode
- Pertence a disciplina Compiladores
- Contem a *gramática melhorada:*
	- `expr` - `term` - `factor` 

## Folder Structure

- `src`: A pasta contém os códigos dos nossos analisadores. Está subdividade no pacote `scanner` e `parser`
- `lib`: A pasta contém o runtime do java-cup-11b-runtime.jar
- `tools`: A pasta não pertence ao projeto propriamente, so coloquei ela la para facilitar a execução do script de geração do scanner e do parser
